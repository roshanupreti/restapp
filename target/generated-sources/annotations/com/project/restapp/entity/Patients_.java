package com.project.restapp.entity;

import com.project.restapp.entity.Reports;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-02-02T12:47:37")
@StaticMetamodel(Patients.class)
public class Patients_ { 

    public static volatile SingularAttribute<Patients, String> firstname;
    public static volatile SingularAttribute<Patients, String> city;
    public static volatile SingularAttribute<Patients, String> street;
    public static volatile SingularAttribute<Patients, Integer> postalcode;
    public static volatile CollectionAttribute<Patients, Reports> reportsCollection;
    public static volatile SingularAttribute<Patients, Integer> id;
    public static volatile SingularAttribute<Patients, String> lastname;

}