package com.project.restapp.entity;

import com.project.restapp.entity.Patients;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-02-02T12:47:37")
@StaticMetamodel(Reports.class)
public class Reports_ { 

    public static volatile SingularAttribute<Reports, String> symptoms;
    public static volatile SingularAttribute<Reports, String> notes;
    public static volatile SingularAttribute<Reports, Patients> patientID;
    public static volatile SingularAttribute<Reports, Date> reportdate;
    public static volatile SingularAttribute<Reports, String> visitreasons;
    public static volatile SingularAttribute<Reports, String> diagnose;
    public static volatile SingularAttribute<Reports, Integer> id;
    public static volatile SingularAttribute<Reports, Short> followupneeded;

}